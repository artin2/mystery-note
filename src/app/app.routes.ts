import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main.component';
import {AboutComponent} from './about/about.component';
import {ArticleComponent} from './article/article.component';
import {CategoriesComponent} from './categories/categories.component';
import { IntroComponent } from './intro.component';
import { ContactComponent } from './contact/contact.component';
import { TestComponent } from './test/test.component';
// Route config let's you map routes to components
const routes: Routes = [
  // map '/persons' to the people list component
  // {
  //   path: 'intro',
  //   component: IntroComponent,
  // },
  {
    path: 'home',
    component: MainComponent,
  },
  {
    path: 'about',
    component: AboutComponent,
  },
  {
    path: 'article/:id',
    component: ArticleComponent
  },

  {
    path: 'category/:name',
    component: CategoriesComponent
  },

  {
    path: 'contact',
    component: ContactComponent
  },
  // make sure to delete when done
  {
    path: 'test',
    component: TestComponent
  },
  // map '/' to '/persons' as our default route
  {
    path: '**',
    redirectTo: '/home',
    pathMatch: 'full'
  },
];

export const appRouterModule = RouterModule.forRoot(routes);
