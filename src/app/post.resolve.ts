import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import { PostfbService } from './services/postfb.service';

@Injectable()
export class PostResolve implements Resolve<any> {

    constructor(private postService: PostfbService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.postService.getPostById(route.params['id']);
  }
}
