import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../services/post.service';
import { Post } from '../models/post';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { PostfbService } from '../services/postfb.service';
import { Category } from '../enums/category.enum';
import { SharedService } from '../services/shared.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],
  providers: [PostfbService, SharedService]
})
export class CategoriesComponent implements OnInit {
  categoryTitle: any;
  category: string;
  posts: Observable<any[]>;
  private sub: Subscription;
  isHover: number;
  showSpinner = true;
  constructor(
    private aRoute: ActivatedRoute,
    private postService: PostfbService,
    private router: Router
  ) { }

  ngOnInit() {
    this.aRoute.params.subscribe(params => {
      this.categoryTitle = params['name'];
      switch (this.categoryTitle) {
        case 'soft-skills': {
          this.category = Category.softSkills;
          break;
        }
        case 'mental-health': {
          this.category = Category.mentalHealth;
          break;
        }
        case 'dev-tips': {
          this.category = Category.devTips;
          break;
        }
      }
      this.postService.getAllForCategory(this.category)
      .subscribe(p => { this.posts = p; },
        err => {console.log('getbycategory error', err); }
      );
      console.log(this.category);
      console.log('categoryTitle', this.categoryTitle);
    }, err => {
      console.log('Error in category route params');
    }, () => {console.log('Category route params complete'); this.showSpinner = false; }
    );
    // this.posts.subscribe( () => this.showSpinner = false);
  }
  showTitle(i: number) {
    this.isHover = i;
  }

  hideTitle() {
    this.isHover = null;
  }

  navigate(id: string) {
    console.log('navigate');
    this.router.navigate([`/article/${id}`]);
    console.log(`/article/${id}`);
  }

  // arrangePosts(arr: any) {
  //   let matrix = [], i, k;

  //   for (i = 0, k = -1; i < arr.length; i++) {
  //       if (i % 3 === 0) {
  //           k++;
  //           matrix[k] = [];
  //       }

  //       matrix[k].push(arr[i]);
  //   }

  //   return matrix;
  // }


}
