import {
  Component, OnInit, Input, OnChanges,
  ElementRef, HostListener, Renderer2
} from '@angular/core';
import { Router } from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { MainComponent } from './main.component';
import { Post } from './models/post';
import { SharedService } from './services/shared.service';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';
import { PostfbService } from './services/postfb.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [PostfbService] // SharedService,
})
export class AppComponent implements OnInit {

  isSearchOpen = false;
  title = 'app';
  href = '';
  isMenuEnabled = false;
  isTitleShown = false;
  posts: Observable<any[]>;
  private postCollection: AngularFirestoreCollection<Post>;
  private isScrolled = false;
  private sectionsIndex: any;
  constructor(
    private el: ElementRef,
    // private sharedService: SharedService,
    private router: Router,
    private db: AngularFirestore,
    private postService: PostfbService,
    private renderer: Renderer2
  ) {
    // sharedService.changeEmitted$.subscribe(
    //   s => { this.sectionsIndex = s; console.log(this.sectionsIndex); }
    // );

    this.postCollection = db.collection<Post>('articles');

  }
  ngOnInit(): void {
    // this.posts = this.postService.getPosts();
    this.posts = this.postService.getAllForCategory('SS');
    let global = this.renderer.listen(this.el.nativeElement, 'click', (evt) => {
      console.log('Clicking the document', evt);
      console.log(evt.toElement);
    });
  }

    // @HostListener('document:click', ['$event'])
    // handleClick(event: Event) {
    //   if (this.el.nativeElement.contains(event.target)) {
    //     console.log('CLICKED');
    //     console.log(this.el.nativeElement);
    //   }
    // }
  // @HostListener('window:scroll', [])
  // onWindowScroll() {
  //   const scrollNumber = window.scrollY;
  //   console.log(scrollNumber);
  //   if (scrollNumber > 195) {
  //     this.isScrolled = true;
  //   } else if (this.isScrolled && scrollNumber < 10) {
  //     this.isScrolled = false;
  //   }
  //   // this.isScrolled = this.isScrolledPast();
  // }

  showMenu() {
    this.isMenuEnabled = !this.isMenuEnabled;
  }

  showTitle() {
    this.isTitleShown = !this.isTitleShown;
    console.log(this.isTitleShown);
  }

  toggleSearch() {
    this.isSearchOpen = !this.isSearchOpen;
    console.log('search toggle', this.isSearchOpen);
  }
  // getAllForCategory(category): any {
  //   return this.db.collection('articles', ref => ref.where('category', '==', 'SS'));
  // }

  // private isScrolledPast(): boolean {
  //   const offset: number = this.el.nativeElement.parentElement.offsetTop - this.el.nativeElement.offsetTop;
  //   const pos: number = this.sectionsIndex.position;
  //   console.log('offset:', offset);
  //   console.log('position:', this.sectionsIndex.position);
  //   console.log('scrollY:', window.scrollY);
  //   console.log('value:', pos - window.scrollY - 195);
  //   if (pos - window.scrollY - 195 < 0) { return true; }
  //   return false;
  // }

}
