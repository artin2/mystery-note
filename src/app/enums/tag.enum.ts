export enum Tag {
  stress = 'Stress',
  productivity = 'Productivity',
  css = 'CSS',
  html = 'HTML',
  design = 'Design',
  depression = 'Depression'
}
