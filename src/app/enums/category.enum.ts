export enum Category {
    softSkills = 'SS',
    mentalHealth = 'MH',
    devTips = 'DT'
}
