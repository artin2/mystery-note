import { Injectable } from '@angular/core';
import { Post } from '../models/post';
import { Params } from '@angular/router';
@Injectable()
export class FilterService {

  get({ data, filter }: Params): Post[] {
    console.log(data);
    return data.filter(post => {
      console.log(data);
      if (post.title && isMatch(post.title, filter)) { return post; }
    });
  }

}

function isMatch(name: string, filter: string): RegExpMatchArray {
  return name.match(new RegExp(filter, 'i'));
}
