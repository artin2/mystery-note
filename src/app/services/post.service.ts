import { Injectable, Inject } from '@angular/core';
import {Post} from '../models/post';

import { Http, RequestOptions, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class PostService {

private baseUrl = 'http://localhost:3000';

constructor(private http: Http) { }

getAll(): Observable<Post[]> {
  return this.http.get(`${this.baseUrl}/posts`, { headers: this.getHeaders() })
    .map(res => res.json())
    .catch((error: any) => Observable.throw(error.json().error || 'Post Service Error'));
}

getPost(id: number): Observable<Post> {
    return this.http
      .get(`${this.baseUrl}/post/${id}`, {headers: this.getHeaders()})
      .map(res => res.json()[0] as Post)
      .catch((error: any) => Observable.throw(error.json().error  || 'Post get by id Error'));
  }

addPost(post: Post) {
  return this.http
    .post(`${this.baseUrl}/posts/addpost`, JSON.stringify(post), {headers: this.getHeaders()} )
    .subscribe(data => console.log(data.json()));
}

getPostsByTag(id: number): Observable<Post[]> {
  return this.http
    .get(`${this.baseUrl}/post/tag/${id}`, {headers: this.getHeaders()})
    .map(res => res.json())
    .catch((error: any) => Observable.throw(error.json().error  || 'Post get by tag Error'));
}
updatePost(post: Post) {
console.log(post.id);
console.log(post.title);
console.log(post.text);
//   return this.http
//     .put(`${this.baseUrl}/folders/rename/${id}`, JSON.stringify(newName), options)
//     //.subscribe(data => console.log(data.json()));
//     .map(res => res.json());
//     //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
}
// renameFolder(id: number, newName: string) {
//   console.log(id);
//   console.log(newName);
//   let options = new RequestOptions({
//     headers: new Headers({ 'Content-Type': 'application/json' })
//   });
//   return this.http
//     .put(`${this.baseUrl}/folders/rename/${id}`, JSON.stringify(newName), options)
//     //.subscribe(data => console.log(data.json()));
//     .map(res => res.json());
//     //.catch((error: any) => Observable.throw(error.json().error || 'Server error'));
// }

private getHeaders() {
  const headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return headers;
}

}
