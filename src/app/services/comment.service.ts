import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Comment } from '../models/comment';
import { Observable } from 'rxjs/Observable';
@Injectable()
export class CommentService {
  private commentCollection: AngularFirestoreCollection<Comment>;
  constructor(private db: AngularFirestore) {
    this.commentCollection = db.collection<Comment>('comments');
  }

  getComments(): Observable<Comment[]> {
    return this.commentCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Comment;
        const id = a.payload.doc.id;
        console.log(id);
        console.log(data);
        return { id, ...data };
      }, err => console.log('Error getComments'));
    });
  }

  saveComment() {

  }

  private postComment() {

  }

}
