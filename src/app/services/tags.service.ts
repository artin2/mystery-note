import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Tag } from '../models/tag';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
@Injectable()
export class TagsService {
  private tagCollection: AngularFirestoreCollection<Tag>;

  constructor(private db: AngularFirestore) {
    this.tagCollection = db.collection<Tag>('tags');
  }

  getAllTags(): Observable<Tag[]>{
    return this.tagCollection.valueChanges();
  }
}
