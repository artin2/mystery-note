import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Post } from '../models/post';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PostfbService {
  private postCollection: AngularFirestoreCollection<Post>;
  readonly path = 'articles';
  constructor(private db: AngularFirestore) {
    this.postCollection = db.collection<Post>(this.path);
  }

  getPosts(): Observable<Post[]> {
    return this.postCollection.snapshotChanges().map(actions => {
      return actions.map(a => {
        const data = a.payload.doc.data() as Post;
        const id = a.payload.doc.id;
        console.log(id);
        console.log(data);
        return { id, ...data };
      }, err => console.log('Error getPosts'));
    });
  }

  getAllForCategory(category: string): any {
    console.log(category);
    return this.db.collection('articles', ref => ref.where('category', '==', category))
      .snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Post;
          const id = a.payload.doc.id;
          console.log('id', id);
          console.log('data', data);
          return { id, ...data };
        }, err => console.log('Error for getAllForCategory'));
      });
  }

  getPostById(postId: string): any {
    console.log('get Post by id:', postId);
    console.log(`${this.path}/dXRLNM720gQzLSkr2Ry3`);
    return this.db.collection('articles')
    .doc(postId).valueChanges();
  }

  getFirstFivePosts(): any {
    return this.db.collection('articles', ref => ref.orderBy('createTime', 'desc').limit(5))
      .snapshotChanges().map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Post;
          const id = a.payload.doc.id;
          console.log('5id', id);
          console.log('5data', data);
          return { id, ...data };
        }, err => {
          console.log('Error getFirstFivePosts');
        });
      });
  }

}
