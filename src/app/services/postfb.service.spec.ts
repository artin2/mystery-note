import { TestBed, inject } from '@angular/core/testing';

import { PostfbService } from './postfb.service';

describe('PostfbService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PostfbService]
    });
  });

  it('should be created', inject([PostfbService], (service: PostfbService) => {
    expect(service).toBeTruthy();
  }));
});
