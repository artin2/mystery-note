import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { appRouterModule } from './app.routes';
import { MainComponent } from './main.component';
import { AboutComponent } from './about/about.component';
import { ArticleComponent } from './article/article.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { CategoriesComponent } from './categories/categories.component';
import { IntroComponent } from './intro.component';
import { ContactComponent } from './contact/contact.component';
import {PostService } from './services/post.service';
import 'rxjs/add/operator/map';
import 'rxjs/Observable';
import 'angular2-navigate-with-data';
import { OneBoxComponent } from './blog-containers/one-box/one-box.component';
import { ThreeBoxComponent } from './blog-containers/three-box/three-box.component';
import { TwoBoxComponent } from './blog-containers/two-box/two-box.component';
import { FourBoxComponent } from './blog-containers/four-box/four-box.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { environment } from '../environments/environment';
import { LeftBoxComponent } from './blog-containers/left-box/left-box.component';
import { RightColumnComponent } from './right-column/right-column.component';
import { TestComponent } from './test/test.component';
import { StickyDirective } from './directives/sticky.directive';
import { TagBoxComponent } from './right-column/tag-box/tag-box.component';
import { AboutMeBoxComponent } from './right-column/about-me-box/about-me-box.component';
import { SocialBoxComponent } from './right-column/social-box/social-box.component';
import { PostBoxComponent } from './right-column/post-box/post-box.component';
import { PostfbService } from './services/postfb.service';
import { SearchComponent } from './search/search.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { CommentsComponent } from './comments/comments.component';
import { PaginationComponent } from './ui/pagination/pagination.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MainComponent,
    AboutComponent,
    ArticleComponent,
    DropdownComponent,
    CategoriesComponent,
    IntroComponent,
    ContactComponent,
    OneBoxComponent,
    ThreeBoxComponent,
    TwoBoxComponent,
    FourBoxComponent,
    LeftBoxComponent,
    RightColumnComponent,
    TestComponent,
    StickyDirective,
    TagBoxComponent,
    AboutMeBoxComponent,
    SocialBoxComponent,
    PostBoxComponent,
    SearchComponent,
    LoadingSpinnerComponent,
    CommentsComponent,
    PaginationComponent,
  ],
  imports: [
    BrowserModule,
    appRouterModule,
    HttpModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule
  ],
  providers: [PostService, PostfbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
