import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { FilterService } from '../services/filter.service';
import { WebStorageService } from '../services/web-storage.service';
import { PostfbService } from '../services/postfb.service';
import { Observable } from 'rxjs/Observable';
import { Post } from '../models/post';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss'],
  providers: [FilterService, WebStorageService, PostfbService ]
})
export class SearchComponent implements OnInit {
  searchForm: FormGroup;
  filter: string;
  posts: Observable<any[]>;
  constructor(
    private filterService: FilterService,
    private webStorageService: WebStorageService,
    private postService: PostfbService
  ) { }

  ngOnInit() {
    this.searchForm = new FormGroup({
      searchField: new FormControl()
    });
    this.posts = this.postService.getPosts();
  }

  onEnter(event: any) {
    if (event.key === 'Enter') {
      console.log('on enter search');
      console.log(this.searchForm.value);
      this.searchForm.reset();
    }
  }

}
