import { Component, OnInit, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { SharedService } from './services/shared.service';
import { PostfbService } from './services/postfb.service';
import { TagsService } from './services/tags.service';
import { Observable } from 'rxjs/Observable';
import { Post } from './models/post';
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  providers: [PostfbService, SharedService, TagsService]
})
export class MainComponent implements OnInit {
  href = '';
  posts: Observable<Post[]>;
  showSpinner = true;
  tags: any;
  constructor(
    private element: ElementRef,
    private sharedService: SharedService,
    private postService: PostfbService,
    private tagsService: TagsService
  ) { }

  ngOnInit() {
    // this.sharedService.emitChange({ position: this.element.nativeElement.offsetTop });
    this.posts = this.postService.getFirstFivePosts();
    this.posts.subscribe(() => this.showSpinner = false);
    this.tagsService.getAllTags()
    .subscribe( t => {
      this.tags = t;
      this.sharedService.emitChange({tags: this.tags});
    });
  }

  // @HostListener('window:resize', ['$event'])
  // onResize(event) {
  //   this.sharedService.emitChange({ position: this.element.nativeElement.offsetTop });
  // }
}
