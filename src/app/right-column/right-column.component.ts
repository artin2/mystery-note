import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-right-column',
  templateUrl: './right-column.component.html',
  styleUrls: ['./right-column.component.scss']
})
export class RightColumnComponent implements OnInit {
  param: string;
  isAboutMeShown: boolean;
  isSocialShown: boolean;
  isPostShown: boolean;
  isTagShown: boolean;

  constructor(
    private aRoute: ActivatedRoute,
    private route: Router
  ) {}

  ngOnInit() {
    console.log(this.route.url);
    this.param = this.route.url;
    this.boxesToShow(this.param);
  }

  boxesToShow(url: string) {
    if (url.includes('home')) {
      console.log('ishome');
      this.isAboutMeShown = true;
      this.isSocialShown = true;
      this.isPostShown = true;
      this.isTagShown = true;
    } else if (url.includes('category')) {
      console.log('iscategory');
      this.isAboutMeShown = true;
      this.isSocialShown = true;
      this.isPostShown = false;
      this.isTagShown = true;
    } else if (url.includes('article')) {
      console.log('isarticle');
      this.isAboutMeShown = false;
      this.isSocialShown = false;
      this.isPostShown = true;
      this.isTagShown = true;
    } else {
      console.log('else');
      this.isAboutMeShown = false;
      this.isSocialShown = false;
      this.isPostShown = false;
      this.isTagShown = false;
    }
  }

}
