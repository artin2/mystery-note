import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutMeBoxComponent } from './about-me-box.component';

describe('AboutMeBoxComponent', () => {
  let component: AboutMeBoxComponent;
  let fixture: ComponentFixture<AboutMeBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutMeBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutMeBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
