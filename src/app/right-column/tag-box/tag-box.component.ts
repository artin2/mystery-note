import { Component, OnInit } from '@angular/core';
import { Tag } from '../../enums/tag.enum';
import { PostfbService } from '../../services/postfb.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Post } from '../../models/post';
import { SharedService } from '../../services/shared.service';
@Component({
  selector: 'app-tag-box',
  templateUrl: './tag-box.component.html',
  styleUrls: ['./tag-box.component.scss'],
  providers: []
})
export class TagBoxComponent implements OnInit {
  tags: string[];
  post: any;
  param: string;
  constructor(
    private aRoute: ActivatedRoute,
    private postService: PostfbService,
    private route: Router,
    private sharedService: SharedService
  ) {
  }

  ngOnInit() {
    console.log('in tagbox', this.route.url);
    this.param = this.route.url;
    this.sharedService.changeEmitted$.subscribe(
      s => {
        console.log('tags', s);
        this.tagsToShow(this.param, s);
        // this.tags = s['post'];
       }, err => console.log('shared service error')
    );
    // this.aRoute.params.subscribe(param => {
    //   console.log(param['id']);
    // });
  }

  tagsToShow(url: string, tags: any) {
    console.log(url, tags);
    if (this.param.includes('article')) {
      console.log('at article');
      this.tags = tags['post'];
    } else if (this.param.includes('category')) {
      console.log('at category');
    } else if (this.param.includes('home')) {
      this.tags = tags['tags'];
    }
  }



}


