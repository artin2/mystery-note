import { Component, OnInit } from '@angular/core';
import { PostfbService } from '../../services/postfb.service';
import { Observable } from 'rxjs/Observable';
import { Post } from '../../models/post';
import { Router, ActivatedRoute, Params } from '@angular/router';
@Component({
  selector: 'app-post-box',
  templateUrl: './post-box.component.html',
  styleUrls: ['./post-box.component.scss']
})
export class PostBoxComponent implements OnInit {
  posts: Observable<Post[]>;
  constructor(
    private postService: PostfbService,
    private router: Router,
    private aRoute: ActivatedRoute) { }

  ngOnInit() {
    this.posts = this.postService.getFirstFivePosts();
  }
  navigate(id: string) {
    console.log('navigate');
    this.router.navigate([`/article/${id}`]);
    console.log(`/article/${id}`);

  }

}
