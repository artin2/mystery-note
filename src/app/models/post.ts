export class Post {
  id: number;
  title: string;
  text: string;
  createTime: Date;
  teaserText: string;
  tags: string[];
}
