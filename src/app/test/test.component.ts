import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.scss']
})
export class TestComponent implements OnInit {

  constructor(private db: AngularFirestore) { }

  ngOnInit() {
  }
  testAddArticle() {
    this.db.collection('articles').add({
      title: 'Mental Health Title4',
      text: 'This is my MH text4',
      category: 'MH',
    });
  }
  testAddComment() {
    this.db.collection('comments').add({
      name: 'Homer Simpson',
      email: 'hsimpson@gmail.com',
      comment: 'Aww 20 dollars...I wanted a peanut'
    });
  }
}
