import { Component, OnInit } from '@angular/core';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  items = [
    'HOME',
    'ARCHIVE',
    'ABOUT'
  ];

  isMenuEnabled = false;
  constructor() { }

  ngOnInit() {
  }

  showMenu() {
    this.isMenuEnabled = !this.isMenuEnabled;
  }

}
