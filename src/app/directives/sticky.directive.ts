import {
  Directive,
  Output,
  EventEmitter,
  HostListener,
  ElementRef,
  OnDestroy
} from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromEvent';

@Directive({
  selector: '[appSticky]'
})
export class StickyDirective implements OnDestroy {

  @Output() scrollPosition: EventEmitter<number> = new EventEmitter<number>
    ();

  private scrollEvent$;

  constructor(private el: ElementRef) {
    console.log('here');
    this.scrollEvent$ = Observable.fromEvent(window,
      'scroll').subscribe((e: any) => {
        console.log('scrollPos: ', e);
        this.onWindowScroll();
        // this.scrollPosition.emit(e.target.scrollTop);
      });
  }
  ngOnDestroy() {
    this.scrollEvent$.unsubscribe();
  }

  onWindowScroll() {
    console.log('here too');
  }

}
