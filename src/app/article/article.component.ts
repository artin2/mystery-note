import { Component, OnInit, Input, ElementRef, Renderer2, ViewChild, AfterViewInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Post } from '../models/post';
import { Tag } from '../models/tag';
import { CommentService } from '../services/comment.service';
import { PostfbService } from '../services/postfb.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from 'angularfire2/firestore';
import { SharedService } from '../services/shared.service';
@Component({
  selector: 'app-article',
  templateUrl: './article.component.html',
  styleUrls: ['./article.component.scss'],
  providers: [CommentService, PostfbService, SharedService] // PostService
})
export class ArticleComponent implements OnInit {
  // @Input() postId: number;
  // @ViewChild('page') el: ElementRef;
  // spost: Observable<Post>;
  private itemCollection: AngularFirestoreCollection<Post>;
  postId: any;
  post: any;
  postByID: any;
  isCommentFormVisible = false;
  isCommentListVisible = false;
  commentForm: FormGroup;
  comments: Observable<any[]>;
  commentSize: any;
  testHTML = '<div> HIIIIIII</div>';
  constructor(
    private rd: Renderer2,
    private aRoute: ActivatedRoute,
    private router: Router,
    private db: AngularFirestore,
    private commentService: CommentService,
    private postService: PostfbService,
    private sharedService: SharedService,
  ) { }

  ngOnInit() {
    this.comments = this.commentService.getComments();
    this.comments.subscribe(len => { this.commentSize = len.length; },
      err => console.log('Error comment length')
    );
    // this.post = this.router.getNavigatedData();
    this.createCommentForm();
    // console.log('post', this.post);
    this.postId = this.aRoute.snapshot.paramMap.get('id');
    this.postService.getPostById(this.postId)
      .subscribe(p => {
          this.postByID = p;
          this.sharedService.emitChange({ post: this.postByID.tags });
        },
        err => console.log('postbyid failed', err));
  }

  showCommentForm() {
    this.isCommentFormVisible = !this.isCommentFormVisible;
  }

  submitComment(value) {
    console.log(value);
    this.db.collection('comments').add({
      name: value.name,
      email: value.email,
      comment: value.comment
    });
    this.showCommentForm();
    this.showCommentList();
  }

  showCommentList() {
    this.isCommentListVisible = !this.isCommentListVisible;

  }

  getComment() {
  }

  createCommentForm() {
    this.commentForm = new FormGroup({
      name: new FormControl(),
      email: new FormControl(),
      comment: new FormControl()
    });
  }

}
