import { Component, OnInit } from '@angular/core';
import { Tag } from '../enums/tag.enum';
@Component({
  // tslint:disable-next-line:component-selector
  selector: 'dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.scss']
})
export class DropdownComponent implements OnInit {
  menuItems = ['HOME', 'DEV TIPS', 'SOFT SKILLS', 'MENTAL HEALTH', 'CONTACT', 'ABOUT'];
  constructor() { }

  ngOnInit() {
    // console.log(Tag.conspiracy);
  }

}
