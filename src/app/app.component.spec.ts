import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';
import { AboutComponent } from './about/about.component';
import { ArticleComponent } from './article/article.component';
import { CategoriesComponent } from './categories/categories.component';
import {NavbarComponent} from './navbar/navbar.component';
import { DropdownComponent } from './dropdown/dropdown.component';
import { APP_BASE_HREF } from '@angular/common';
describe('AppComponent', () => {
  const routes: Routes = [
    {
      path: 'home',
      component: MainComponent,
    },
    {
      path: 'about',
      component: AboutComponent,
    },
    {
      path: 'article/:id',
      component: ArticleComponent
    },
    {
      path: 'archive',
      component: CategoriesComponent
    },
    // map '/' to '/persons' as our default route
    {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full'
    },
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        MainComponent,
        AboutComponent,
        ArticleComponent,
        CategoriesComponent,
        NavbarComponent,
        DropdownComponent
      ],
      imports: [
        RouterModule.forRoot(routes)
      ],
      providers: [
        {provide: APP_BASE_HREF, useValue: '/'}
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'app'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('app');
  }));
  it('should render title in a h1 tag', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Welcome to app!');
  }));
});
