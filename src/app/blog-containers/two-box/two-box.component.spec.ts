import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TwoBoxComponent } from './two-box.component';

describe('TwoImgComponent', () => {
  let component: TwoBoxComponent;
  let fixture: ComponentFixture<TwoBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TwoBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TwoBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
