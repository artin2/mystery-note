import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FourBoxComponent } from './four-box.component';

describe('FourBoxComponent', () => {
  let component: FourBoxComponent;
  let fixture: ComponentFixture<FourBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FourBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FourBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
