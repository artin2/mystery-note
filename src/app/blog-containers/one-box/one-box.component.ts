import { Component, OnInit, Input } from '@angular/core';
import { Post } from '../../models/post';
import {Router} from '@angular/router';
import { Category } from '../../enums/category.enum';
@Component({
  selector: 'app-one-box',
  templateUrl: './one-box.component.html',
  styleUrls: ['./one-box.component.scss']
})
export class OneBoxComponent implements OnInit {
  @Input() title;
  @Input() text;
  @Input() category;
  @Input() id;
  @Input() post;
  categoryDisplay: string;
  image: string;
  constructor(private router: Router) { }

  ngOnInit() {
    switch (this.category) {
      case 'SS': {
        this.categoryDisplay = 'Soft Skills';
        break;
      }
      case 'MH': {
        this.categoryDisplay = 'Mental Health';
        break;
      }
      case 'DT': {
        this.categoryDisplay = 'Dev Tips';
        break;
      }
    }
    console.log(this.categoryDisplay);
    this.image = '../../../assets/imgs/dummy/desk.jpg';
  }

  navigate() {
    console.log('navigate');
    this.router.navigate([`/article/${this.id}`]);
    console.log(`/article/${this.id}`);
  }
}
