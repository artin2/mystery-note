// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBJ71XMwPtKXxk_zZ3UxoQYmoG41SNSO60',
    authDomain: 'my-blog-95451.firebaseapp.com',
    databaseURL: 'https://my-blog-95451.firebaseio.com',
    projectId: 'my-blog-95451',
    storageBucket: 'staging.my-blog-95451.appspot.com',
    messagingSenderId: '336753697366'
  }
};
