// var express = require('express');
// var app = express();

// // Import User Module Containing Functions Related To User Data
// var post = require('../models/posts');

// // API Routes

// //GET all post
// app.get('/posts', function(req, res) {
// 	post.findAll(function(err, rows, fields) {
// 		if(err) throw err;
// 		res.json(rows);
// 	})
// });

// //GET post by Id
// app.get('/post/:id', function(req, res) {
// 	post.findByPostId(req.params, function(err, rows, fields) {
// 		if(err) throw err;
// 		res.json(rows);
// 	})
// });

// app.get('/post/tag/:id', function(req, res) {
// 	post.findByTagId(req.params, function(err, rows, fields) {
// 		if(err) throw err;
// 		res.json(rows);
// 	})
// });
// //GET recent posts
// app.get('/posts/recent', function(req, res) {
// 	post.findRecentPosts(function(err, rows, fields) {
// 		if(err) throw err;
// 		res.json(rows);
// 	})
// });

// //rename by ID
// app.put('/posts/update/:id' , function(req, res) {
// 	console.log("in API");
// 	console.log(req.body);
// 	console.log("id", req.params);
// 	var data = [req.params, req.body];
// 	post.updatePost(data, function(err, rows, fields) {
// 		if(err) throw err;
// 		res.json(rows);
// 	});
// });


// //add Post
// app.post('/posts/addpost', function(req, res) {
// 	var data = req.body;
//   console.log("req data" + data);
//   post.addPost(data, function(err, info) {
//     if(err) throw err;
//     post.sendResponse(true, res);
//   });
// });

// //all other requests redirect to 404 
// //TODO need to fix header error - probably something with next();
// // app.all("*", function(req, res, next) {
// // 	res.send("page not found");
// // 	next();
// // });



// module.exports = app;
